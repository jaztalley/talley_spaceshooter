﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : MonoBehaviour
{

    private Rigidbody rb;
    public GameObject[] junkSpeed;
    private bool speedup = false;
    private GameController gameController;
    public bool shootForFast;



    void Start()
    {

        rb = GetComponent<Rigidbody>();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (speedup != true && gameController.FastStuff)
        {
            rb.velocity *= 1.4f;
            speedup = true;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (shootForFast && gameController.score >= 5)
        {
            gameController.SlowStuff = true;
        }

    }
}