﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDown : MonoBehaviour {

    private Rigidbody rb;
    public GameObject[] veggieSlow;
    private bool slowDown = false;
    private GameController gameController;
    public bool shootToSlow;

    


    void Start()
    {

        rb = GetComponent<Rigidbody>();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    // Update is called once per frame
    void Update() {

        if (slowDown != true && gameController.SlowStuff)
        {
            rb.velocity *= 0.4f;
            slowDown = true;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (shootToSlow && gameController.score >= 20)
        {
            gameController.SlowStuff = true;
        }

    }
}